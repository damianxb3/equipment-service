import { EquipmentServicePage } from './app.po';

describe('equipment-service App', () => {
  let page: EquipmentServicePage;

  beforeEach(() => {
    page = new EquipmentServicePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { CategoryService } from "./category/category.service";
import { CategoryComponent } from "./category/category-list.component";
import { DeviceListComponent } from './device/device-list.component';
import { DeviceService } from "./device/device.service";
import { CommentListComponent } from './comment/comment-list.component';
import { ParameterListComponent } from './parameter/parameter-list.component';
import { CommentBoxComponent } from './comment-box/comment-box.component';
import { NewDeviceFormComponent } from './device/new-device-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    DeviceListComponent,
    CommentListComponent,
    ParameterListComponent,
    CommentBoxComponent,
    NewDeviceFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [CategoryService, DeviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }

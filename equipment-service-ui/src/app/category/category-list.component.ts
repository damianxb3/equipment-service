import { Component, OnInit } from '@angular/core';
import {CategoryService} from "./category.service";
import {Category} from "../model";

@Component({
  selector: 'app-category',
  templateUrl: 'category-list.component.html'
})
export class CategoryComponent implements OnInit {
  title = "Kategorie";
  categories: Category[];

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.categoryService.getAllCategories()
      .then(categories => this.categories = categories);
  }

}

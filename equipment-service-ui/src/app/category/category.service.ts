import { Injectable } from '@angular/core';
import {Category} from "../model";
import {Http} from "@angular/http";

import 'rxjs/add/operator/toPromise'

import {apiUrl} from "../conf"

@Injectable()
export class CategoryService {
  private categoriesUrl = apiUrl + "/category";
  private allCategoriesUrl = this.categoriesUrl + "/all";

  constructor(private http: Http) { }

  getAllCategories(): Promise<Category[]> {
    return this.http.get(this.allCategoriesUrl)
      .toPromise()
      .then(response => response.json() as Category[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}

import {Component, OnInit, Input} from '@angular/core';
import {DeviceService} from "../device/device.service";
import {Device} from "../model";
import {UpdatedDeviceDto} from "../dto/updatedDevice.dto";

@Component({
  selector: 'app-comment-box',
  templateUrl: './comment-box.component.html',
  styleUrls: ['./comment-box.component.css']
})
export class CommentBoxComponent implements OnInit {

  commentBody: string;
  commentAuthor: string;
  @Input() device: Device;
  isSuccessInfoVisible = false;

  constructor(private deviceService: DeviceService) { }

  ngOnInit() {
  }

  sendComment() {

    this.device.comments.push({
      author: this.commentAuthor,
      body: this.commentBody,
      date: new Date()
    });

    this.commentAuthor = "";
    this.commentBody = "";

    let updatedDeviceDto: UpdatedDeviceDto = {
      id: this.device.id,
      name: this.device.name,
      damaged: this.device.damaged,
      categoryId: this.device.category.id,
      parameters: this.device.parameters,
      comments: this.device.comments
    };

    this.deviceService.updateDevice(updatedDeviceDto)
      .then(status => this.showUpdateStatus(status));
  }

  private showUpdateStatus(status: number) {
    if (status == 200) {
      this.isSuccessInfoVisible = true;
      setTimeout(() => {console.log("working"); this.isSuccessInfoVisible = false}, 3000);
    }
  }

}

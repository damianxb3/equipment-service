import {Component, OnInit, Input} from '@angular/core';
import {Device} from "../model";

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html'
})
export class CommentListComponent implements OnInit {

  @Input() device: Device;

  constructor() {
  }

  ngOnInit() {
  }

}

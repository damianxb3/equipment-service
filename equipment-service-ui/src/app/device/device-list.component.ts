import {Component, OnInit} from '@angular/core';
import {DeviceService} from "./device.service";
import {Device} from "../model";
import {UpdatedDeviceDto} from "../dto/updatedDevice.dto";

@Component({
  selector: 'app-device-list',
  templateUrl: 'device-list.component.html'
})
export class DeviceListComponent implements OnInit {
  title = "Urządzenia";
  devices: Device[];
  chosenDevice: Device;
  isCommentsVisible = false;
  isParametersVisible = false;
  isNewDeviceFormVisible = false;

  constructor(private deviceService: DeviceService) {
  }

  ngOnInit() {
    this.deviceService.getAllDevices()
      .then(devices => this.devices = devices);
  }

  reportFailure(device: Device) {
    device.damaged = true;
    this.updateDevice(device);
  }

  removeFailure(device: Device) {
    device.damaged = false;
    this.updateDevice(device);
  }

  private updateDevice(device: Device) {
    let updatedDeviceDto: UpdatedDeviceDto = {
      id: device.id,
      name: device.name,
      damaged: device.damaged,
      categoryId: device.category.id,
      parameters: device.parameters,
      comments: device.comments
    };
    this.deviceService.updateDevice(updatedDeviceDto);
  }

  showParameters(device: Device) {
    this.chosenDevice = device;
    this.isParametersVisible = true;
    this.isCommentsVisible = false;
    this.isNewDeviceFormVisible = false;
  }

  showComments(device: Device) {
    this.chosenDevice = device;
    this.isCommentsVisible = true;
    this.isParametersVisible = false;
    this.isNewDeviceFormVisible = false;
  }

  showNewDeviceForm() {
    this.isNewDeviceFormVisible = true;
    this.isCommentsVisible = false;
    this.isParametersVisible = false;
  }
}

import { Injectable } from '@angular/core';
import {Device} from "../model";
import {Http} from "@angular/http";

import 'rxjs/add/operator/toPromise'

import {apiUrl} from "../conf"
import {UpdatedDeviceDto} from "../dto/updatedDevice.dto";
import {NewDeviceDto} from "../dto/newDevice.dto";

@Injectable()
export class DeviceService {
  private devicesUrl = apiUrl + "/device";
  private allDevicesUrl = this.devicesUrl + "/all";
  private updateDeviceUrl = this.devicesUrl + "/update";
  private createDeviceUrl = this.devicesUrl + "/create";

  constructor(private http: Http) { }

  getAllDevices(): Promise<Device[]> {
    return this.http.get(this.allDevicesUrl)
      .toPromise()
      .then(response => response.json() as Device[])
      .catch(this.handleError);
  }


  updateDevice(device: UpdatedDeviceDto): Promise<any> {
    return this.http.post(this.updateDeviceUrl, device)
      .toPromise()
      .then(response => response.status)
      .catch(this.handleError);
  }

  createDevice(device: NewDeviceDto) {
    return this.http.put(this.createDeviceUrl, device)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}

import {Component, OnInit, Input} from '@angular/core';
import {CategoryService} from "../category/category.service";
import {Category, Device, Parameter} from "../model";
import {DeviceService} from "./device.service";
import {NewDeviceDto} from "../dto/newDevice.dto";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-new-device-form',
  templateUrl: 'new-device-form.component.html'
})
export class NewDeviceFormComponent implements OnInit {

  @Input() devices: Device[];
  categories: Category[];
  newDevice: Device = new Device();

  constructor(
    private categoryService: CategoryService,
    private deviceService: DeviceService) {
    this.newDevice.parameters = [];
  }

  ngOnInit() {
    this.categoryService.getAllCategories()
      .then(categories => this.categories = categories);
  }

  addNewParameter() {
    this.newDevice.parameters.push(new Parameter());
  }

  removeParameter(parameter: Parameter) {
    let index = this.newDevice.parameters.indexOf(parameter);
    if (index > -1) {
      this.newDevice.parameters.splice(index, 1);
    }
  }

  onSubmit() {

    this.newDevice.damaged = false;
    this.newDevice.comments = [];

    let newDeviceListItem: Device = {
      id: null,
      name: this.newDevice.name,
      damaged: this.newDevice.damaged,
      category: this.newDevice.category,
      parameters: [],
      comments: []
    };

    let newDeviceDto: NewDeviceDto = {
      name: this.newDevice.name,
      categoryId: this.newDevice.category.id,
      damaged: this.newDevice.damaged,
      parameters: [],
      comments: []
    };

    for (let parameter of this.newDevice.parameters) {
      newDeviceListItem.parameters.push(parameter);
      newDeviceDto.parameters.push(parameter);
    }

    this.deviceService.createDevice(newDeviceDto)
      .then(newId => {
        newDeviceListItem.id = newId;
        this.devices.push(newDeviceListItem);
      });
  }
}

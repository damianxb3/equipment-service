import {Parameter, Comment} from "../model";

export class NewDeviceDto {
  name: string;
  categoryId: number;
  damaged: boolean;
  parameters: Parameter[];
  comments: Comment[];
}

import {Parameter, Comment} from "../model";

export class UpdatedDeviceDto {
  id: number;
  name: string;
  categoryId: number;
  damaged: boolean;
  parameters: Parameter[];
  comments: Comment[];
}

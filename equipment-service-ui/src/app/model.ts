export class Category {
  id: number;
  name: string;
}

export class Comment {
  author: string;
  body: string;
  date: Date;
}

export class Parameter {
  key: string;
  value: string;
}

export class Device {
  id: number;
  name: string;
  damaged: boolean;
  category: Category;
  parameters: Parameter[];
  comments: Comment[];
}

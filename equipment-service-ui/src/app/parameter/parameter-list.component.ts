import {Component, OnInit, Input} from '@angular/core';
import {Device} from "../model";

@Component({
  selector: 'app-parameter-list',
  templateUrl: './parameter-list.component.html'
})
export class ParameterListComponent implements OnInit {

  @Input() device: Device;

  constructor() { }

  ngOnInit() {
  }

}

package com.damianxb3.category;

import com.damianxb3.category.dto.NewCategoryDto;
import com.damianxb3.category.dto.UpdatedCategoryDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {
    private CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PutMapping("/create")
    @ResponseStatus(value = HttpStatus.OK)
    public void create(@RequestBody NewCategoryDto newCategoryData) {
        categoryService.create(newCategoryData);
    }

    @GetMapping("/all")
    public Iterable<Category> getAll() {
        return categoryService.getAll();
    }

    @GetMapping("/{categoryId}")
    public Category get(@PathVariable long categoryId) {
        return categoryService.get(categoryId);
    }

    @PostMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody UpdatedCategoryDto updatedCategoryDto) {
        categoryService.update(updatedCategoryDto);
    }

    @DeleteMapping("/delete/{categoryId}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable long categoryId) {
        categoryService.delete(categoryId);
    }

}

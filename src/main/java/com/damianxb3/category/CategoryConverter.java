package com.damianxb3.category;

import com.damianxb3.category.dto.NewCategoryDto;
import com.damianxb3.category.dto.UpdatedCategoryDto;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter {
    public Category convert(NewCategoryDto newCategoryDto) {
        String name = newCategoryDto.getName();
        return new Category(name);
    }

    public Category convert(UpdatedCategoryDto updatedCategoryDto) {
        Long id = updatedCategoryDto.getId();
        String name = updatedCategoryDto.getName();
        Category category = new Category(name);
        category.setId(id);
        return category;
    }
}

package com.damianxb3.category;

import com.damianxb3.category.dto.NewCategoryDto;
import com.damianxb3.category.dto.UpdatedCategoryDto;

public interface CategoryService {
    Iterable<Category> getAll();

    void create(NewCategoryDto newCategoryData);

    Category get(long categoryId);

    void update(UpdatedCategoryDto updatedCategoryDto);

    void delete(long categoryId);
}

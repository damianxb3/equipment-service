package com.damianxb3.category;

import com.damianxb3.category.dto.NewCategoryDto;
import com.damianxb3.category.dto.UpdatedCategoryDto;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;
    private CategoryConverter categoryConverter;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryConverter categoryConverter) {
        this.categoryRepository = categoryRepository;
        this.categoryConverter = categoryConverter;
    }

    @Override
    public Iterable<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public void create(NewCategoryDto newCategoryData) {
        Category newCategoryEntity = categoryConverter.convert(newCategoryData);
        categoryRepository.save(newCategoryEntity);
    }

    @Override
    public Category get(long categoryId) {
        return categoryRepository.findOne(categoryId);
    }

    @Override
    public void update(UpdatedCategoryDto updatedCategoryDto) {
        Category updatedCategoryEntity = categoryConverter.convert(updatedCategoryDto);
        categoryRepository.save(updatedCategoryEntity);
    }

    @Override
    public void delete(long categoryId) {
        categoryRepository.delete(categoryId);
    }
}

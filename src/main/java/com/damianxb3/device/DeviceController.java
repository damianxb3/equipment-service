package com.damianxb3.device;

import com.damianxb3.device.domain.Device;
import com.damianxb3.device.dto.NewDeviceDto;
import com.damianxb3.device.dto.UpdatedDeviceDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/device")
public class DeviceController {
    private DeviceService deviceService;

    public DeviceController(DeviceService categoryService) {
        this.deviceService = categoryService;
    }

    @PutMapping("/create")
    public long create(@RequestBody NewDeviceDto newDeviceData) {
        return deviceService.create(newDeviceData);
    }

    @GetMapping("/all")
    public Iterable<Device> getAll() {
        return deviceService.getAll();
    }

    @GetMapping("/{deviceId}")
    public Device get(@PathVariable long deviceId) {
        return deviceService.get(deviceId);
    }

    @PostMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody UpdatedDeviceDto updatedDeviceData) {
        deviceService.update(updatedDeviceData);
    }

    @DeleteMapping("/delete/{deviceId}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable long deviceId) {
        deviceService.delete(deviceId);
    }
}

package com.damianxb3.device;

import com.damianxb3.category.Category;
import com.damianxb3.category.CategoryService;
import com.damianxb3.device.domain.Comment;
import com.damianxb3.device.domain.Device;
import com.damianxb3.device.domain.Parameter;
import com.damianxb3.device.dto.CommentDto;
import com.damianxb3.device.dto.NewDeviceDto;
import com.damianxb3.device.dto.ParameterDto;
import com.damianxb3.device.dto.UpdatedDeviceDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DeviceConverter {
    private CategoryService categoryService;

    public DeviceConverter(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public Device convert(NewDeviceDto newDeviceDto) {
        String name = newDeviceDto.getName();
        long categoryId = newDeviceDto.getCategoryId();
        boolean isDamaged = newDeviceDto.isDamaged();
        Category category = categoryService.get(categoryId);
        List<Parameter> parameters = convertParameters(newDeviceDto.getParameters());
        List<Comment> comments = convertComments(newDeviceDto.getComments());
        return new Device(name, category, isDamaged, parameters, comments);
    }

    public Device convert(UpdatedDeviceDto updatedDeviceDto) {
        long id = updatedDeviceDto.getId();
        String name = updatedDeviceDto.getName();
        long categoryId = updatedDeviceDto.getCategoryId();
        boolean isDamaged = updatedDeviceDto.isDamaged();
        Category category = categoryService.get(categoryId);
        List<Parameter> parameters = convertParameters(updatedDeviceDto.getParameters());
        List<Comment> comments = convertComments(updatedDeviceDto.getComments());
        Device device = new Device(name, category, isDamaged, parameters, comments);
        device.setId(id);
        return device;
    }

    private List<Parameter> convertParameters(List<ParameterDto> parametersData) {
        return parametersData.stream()
                .map(p -> new Parameter(p.getKey(), p.getValue()))
                .collect(Collectors.toList());
    }

    private List<Comment> convertComments(List<CommentDto> commentsData) {
        return commentsData.stream()
                .map(c -> new Comment(c.getAuthor(), c.getBody(), c.getDate()))
                .collect(Collectors.toList());
    }
}

package com.damianxb3.device;

import com.damianxb3.device.domain.Device;
import org.springframework.data.repository.CrudRepository;


public interface DeviceRepository extends CrudRepository<Device, Long> {
}

package com.damianxb3.device;

import com.damianxb3.device.domain.Device;
import com.damianxb3.device.dto.NewDeviceDto;
import com.damianxb3.device.dto.UpdatedDeviceDto;

public interface DeviceService {
    long create(NewDeviceDto newDeviceData);

    Iterable<Device> getAll();

    Device get(long deviceId);

    void update(UpdatedDeviceDto updatedDeviceData);

    void delete(long deviceId);
}

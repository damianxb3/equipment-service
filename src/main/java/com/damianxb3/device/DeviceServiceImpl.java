package com.damianxb3.device;

import com.damianxb3.device.domain.Device;
import com.damianxb3.device.dto.NewDeviceDto;
import com.damianxb3.device.dto.UpdatedDeviceDto;
import org.springframework.stereotype.Service;

@Service
public class DeviceServiceImpl implements DeviceService {
    private DeviceRepository deviceRepository;
    private DeviceConverter deviceConverter;

    public DeviceServiceImpl(DeviceRepository deviceRepository, DeviceConverter deviceConverter) {
        this.deviceRepository = deviceRepository;
        this.deviceConverter = deviceConverter;
    }

    @Override
    public long create(NewDeviceDto newDeviceData) {
        Device newDeviceEntity = deviceConverter.convert(newDeviceData);
        return deviceRepository.save(newDeviceEntity).getId();
    }

    @Override
    public Iterable<Device> getAll() {
        return deviceRepository.findAll();
    }

    @Override
    public Device get(long deviceId) {
        return deviceRepository.findOne(deviceId);
    }

    @Override
    public void update(UpdatedDeviceDto updatedDeviceData) {
        Device updatedDeviceEntity = deviceConverter.convert(updatedDeviceData);
        deviceRepository.save(updatedDeviceEntity);
    }

    @Override
    public void delete(long deviceId) {
        deviceRepository.delete(deviceId);
    }
}

package com.damianxb3.device.domain;

import javax.persistence.Embeddable;
import java.util.Date;

@Embeddable
public class Comment {
    private String author;
    private String body;
    private Date date;

    protected Comment() {
    }

    public Comment(String author, String body, Date date) {
        this.author = author;
        this.body = body;
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

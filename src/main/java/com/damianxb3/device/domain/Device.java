package com.damianxb3.device.domain;

import com.damianxb3.category.Category;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne
    private Category category;

    private boolean damaged;

    @ElementCollection
    @CollectionTable(name="device_parameter")
    private List<Parameter> parameters = new ArrayList<>();

    @ElementCollection
    @CollectionTable(name="device_comment")
    private List<Comment> comments = new ArrayList<>();

    protected Device() {
    }

    public Device(String name, Category category, boolean damaged, List<Parameter> parameters, List<Comment> comments) {
        this.name = name;
        this.category = category;
        this.damaged = damaged;
        this.parameters = parameters;
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isDamaged() {
        return damaged;
    }

    public void setDamaged(boolean damaged) {
        this.damaged = damaged;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}

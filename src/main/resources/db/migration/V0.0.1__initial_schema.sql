CREATE TABLE category
(
  id          SERIAL PRIMARY KEY,
  name        VARCHAR(50) NOT NULL,

  UNIQUE (name)
);

CREATE TABLE device
(
  id          SERIAL PRIMARY KEY,
  name        VARCHAR(50) NOT NULL,
  category_id INTEGER NOT NULL REFERENCES category (id),
  damaged     BOOLEAN DEFAULT (FALSE) NOT NULL,

  UNIQUE (name)
);

CREATE TABLE device_comment
(
  device_id   INTEGER NOT NULL REFERENCES device (id),
  author      VARCHAR(30),
  date        DATE,
  body        TEXT NOT NULL
);

CREATE TABLE device_parameter
(
  device_id   INTEGER REFERENCES device (id),
  key         VARCHAR(50) NOT NULL,
  value       VARCHAR(50) NOT NULL,

  UNIQUE (device_id, key)
);